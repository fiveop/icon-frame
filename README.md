# Icon Frame

The SVG file contained in this repository represents a frame for icon graphics
that work in square and circle shaped contexts.

An explanation of how the shape was determined can be read on the following
webpage:

https://fiveop.de/blog/icon-dimensions.html

## License

To the extent possible under law, Philipp Matthias Schäfer has waived all
copyright and related or neighboring rights to Icon Frame. This work
is published from: Germany.
